#!/usr/bin/env bash

set -x

REPOS_FILE=$1

/opt/docker/bin/scala-steward \
    -DROOT_LOG_LEVEL=INFO \
    -DLOG_LEVEL=INFO \
    --disable-sandbox \
    --do-not-fork \
    --workspace "$(pwd)/workspace/" \
    --repos-file "$(pwd)/$REPOS_FILE" \
    --git-ask-pass "$(pwd)/pass.sh" \
    --git-author-name "Scala Steward" \
    --git-author-email "steward@scala.com" \
    --forge-type "gitlab" \
    --forge-api-host "https://gitlab.com/api/v4/" \
    --forge-login "tyrcho"
